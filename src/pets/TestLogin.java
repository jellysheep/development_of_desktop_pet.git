package pets;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import Gui.SignGui;
import User.User;
import User.UserDaoImpl;

public class TestLogin {
	private static ActionListener listener;

	public void gui() {

		JFrame frame = new JFrame("宠物登录");// 框架

		// 字体

		// 标签

		JLabel label = new JLabel("账号");
		JLabel label1 = new JLabel("密码");
		JLabel label2 = new JLabel("宠之物语");
		JLabel label3 = new JLabel(" ");
		label.setFont(new Font("宋体", Font.BOLD | Font.ITALIC, 20));
		label1.setFont(new Font("宋体", Font.BOLD | Font.ITALIC, 20));
		label2.setFont(new Font("宋体", Font.BOLD | Font.ITALIC, 30));
		// label2.setHorizontalAlignment(JTextField.RIGHT);
		// 文本框
		JTextField txtName = new JTextField();// 输入的账号
		JPasswordField pwdPWD = new JPasswordField();// 输入的密码
		JButton btnSub = new JButton();
		JButton btnReset = new JButton();
		JButton SignUp = new JButton();

		SignUp.setText("注册");
		btnReset.setText("重置");
		btnSub.setText("登录");

		frame.setBounds(780, 300, 60, 20);
		label.setBounds(130, 177, 60, 50);
		label2.setBounds(230, 90, 150, 30);
		SignUp.setBounds(360, 260, 60, 35);
		txtName.setBounds(200, 180, 230, 30);
		pwdPWD.setBounds(200, 215, 230, 30);
		btnSub.setBounds(200, 260, 60, 35);
		btnReset.setBounds(280, 260, 60, 35);
		label1.setBounds(130, 213, 60, 30);
		ImageIcon img = new ImageIcon("界面2.gif");// 这是背景图片
		JLabel imgLabel = new JLabel(img);// 将背景图放在标签里。

		frame.getLayeredPane().add(imgLabel, new Integer(Integer.MIN_VALUE));
		imgLabel.setBounds(0, 0, img.getIconWidth(), img.getIconHeight());
		Container cp = frame.getContentPane();
		cp.setLayout(new BorderLayout());

		cp.add(label2);
		cp.add(SignUp);
		cp.add(btnReset);
		cp.add(txtName);
		cp.add(pwdPWD);
		cp.add(btnSub);
		cp.add(label);
		cp.add(label1);
		cp.add(label3);
		((JPanel) cp).setOpaque(false);

		frame.setUndecorated(true); // 取消窗口标题栏
		frame.setVisible(true);

		// 大小

		frame.setSize(554, 614);

		// 登录按钮的事件监听

		btnSub.addActionListener(listener);
		btnSub.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				UserDaoImpl ud = new UserDaoImpl();
				User user = ud.isLogin(txtName.getText(), pwdPWD.getText());
				if (pwdPWD.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "密码不能为空！");
				} else if (user != null) {
					frame.setVisible(false);// 隐藏掉登陆界面
					new TestBody(user).frame.setLocationRelativeTo(frame);

				} else {
					JOptionPane.showMessageDialog(null, "密码错误！");
				}

			}
		});

		// 重置按钮的事件监听
		btnReset.addActionListener(listener);
		btnReset.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				txtName.setText("");
				pwdPWD.setText("");

			}
		});
		SignUp.addActionListener(listener);
		SignUp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				new SignGui().setVisible(true);
			}

		});
		Point origin = new Point();
		frame.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				// 当鼠标按下的时候获得窗口当前的位置
				origin.x = e.getX();
				origin.y = e.getY();

//                
			}

			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// 处理鼠标移入
				Cursor coursor = Toolkit.getDefaultToolkit().createCustomCursor(new ImageIcon("mouse.png").getImage(),
						new Point(10, 20), "stick");
				frame.setCursor(coursor);
			}
		});

		frame.addMouseMotionListener(new MouseMotionAdapter() {
			// 拖动（mouseDragged 指的不是鼠标在窗口中移动，而是用鼠标拖动）
			public void mouseDragged(MouseEvent e) {
				// 当鼠标拖动时获取窗口当前位置
//                test2.start();
				Point p = frame.getLocation();
				// 设置窗口的位置
				// 窗口当前的位置 + 鼠标当前在窗口的位置 - 鼠标按下的时候在窗口的位置
				int x = p.x + e.getX() - origin.x;
				int y = p.y + e.getY() - origin.y;
				frame.setLocation(x, y);

			}

		});

	}

}
