package pets;

import javax.swing.*;
import set.Setting;
import User.User;
import javax.swing.*;

import Gui.AccounterGui;
import Gui.DialogGui;
import Pet.PetDao;
import User.User;

import java.awt.*;
import java.awt.dnd.*;
import java.awt.event.*;

public class TestBody {
	static JFrame frame;
	private static JLabel jLabel;
	Thread test = new Thread(new Def());
	Thread test2 = new Thread(new Cli());
	boolean flag1 = true;

//    boolean flag2=true;
	public TestBody() {

	}

	public TestBody(User user) {
		frame = new JFrame("digimonDesktopPet");
		// 设置随机位置并显示在最前端
		frame.setLocation((int) (Math.random() * 100), (int) (Math.random() * 100));// 注意int强转时后面把括号也带上
		frame.setAlwaysOnTop(true);

		frame.setSize(200, 200);
		frame.getContentPane().setLayout(null);
		frame.setTitle("测试动画");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setType(JFrame.Type.UTILITY);
		// 动画的承载体
		setTray(user);
		jLabel = new JLabel();
		// 初始化第一张图
		cgJLabelImg(jLabel, "res/default/skeleton-standing0.png");
		frame.add(jLabel);
//        体透明
		frame.setUndecorated(true); // 取消窗口标题栏
		frame.setBackground(new Color(0, 0, 0, 0));// 背frame
		frame.setVisible(true);
		Point origin = new Point();

		frame.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				// 当鼠标按下的时候获得窗口当前的位置
				origin.x = e.getX();
				origin.y = e.getY();
				flag1 = false;
//               
				if (e.getClickCount() == 2) {
					PetDao pet = new PetDao(user);
					pet.addLiking(pet, 1);
				}
			}

			public void mouseReleased(MouseEvent e) {
				flag1 = true;
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// 处理鼠标移入
				Cursor coursor = Toolkit.getDefaultToolkit().createCustomCursor(new ImageIcon("mouse.png").getImage(),
						new Point(10, 20), "stick");
				frame.setCursor(coursor);
			}
		});

		frame.addMouseMotionListener(new MouseMotionAdapter() {
			// 拖动（mouseDragged 指的不是鼠标在窗口中移动，而是用鼠标拖动）
			public void mouseDragged(MouseEvent e) {
				// 当鼠标拖动时获取窗口当前位置
//                test2.start();
				Point p = frame.getLocation();
				// 设置窗口的位置
				// 窗口当前的位置 + 鼠标当前在窗口的位置 - 鼠标按下的时候在窗口的位置
				int x = p.x + e.getX() - origin.x;
				int y = p.y + e.getY() - origin.y;
				frame.setLocation(x, y);

			}

		});

		// 动画线程
		test.start();
		// 弹窗线程

		test2.start();
		// 构造结束
	}

	class Def implements Runnable {
		@Override
		public void run() {
			int i = 1;
			int j = 1;
			try {
				while (true) {
					Thread.sleep(50);
					if (flag1 == true)
						cgJLabelImg(jLabel, "res/default/skeleton-standing" + i++ + ".png");
					else
						cgJLabelImg(jLabel, "res/grabed/skeleton-grabed" + i++ + ".png");
					if (i > 60) {
						i = 1;
					}
					j++;
					if (j > 600) {
						i = 1;
						j = 0;
						int x = 0;
						while (true) {
							Thread.sleep(50);
							if (flag1 == true)
								cgJLabelImg(jLabel, "res/walking/skeleton-walking" + i++ + ".png");
							else
								cgJLabelImg(jLabel, "res/grabed/skeleton-grabed" + i++ + ".png");

							if (i > 60) {
								i = 1;

								x++;
							}

							if (x == 5) {
								break;
							}
						}

					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	class Cli implements Runnable {
		@Override
		public void run() {
			try {
				while (true) {
					Thread.sleep(5000);
					DialogGui a = new DialogGui();
					a.frame.setLocationRelativeTo(frame);
					Thread.sleep(5000);
					a.setV();// 隐藏掉登陆界面
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public Point coordinate() {// 获取宠物当前坐标
		Point p = new Point();
		p.x = frame.getX();
		p.y = frame.getY();
		return p;
	}

	private void cgJLabelImg(JLabel jLabel, String imgUrl) {
		ImageIcon icon = new ImageIcon(imgUrl);
		int picWidth = icon.getIconWidth(), pinHeight = icon.getIconHeight();
		icon.setImage(icon.getImage().getScaledInstance(picWidth, pinHeight, Image.SCALE_DEFAULT));
		jLabel.setBounds(0, 0, picWidth, pinHeight);
		jLabel.setIcon(icon);
	}

	private void setTray(User user) {
		if (SystemTray.isSupported()) {// 判断系统是否支持系统托盘
			SystemTray tray = SystemTray.getSystemTray(); // 获取当前系统的托盘

			// 为托盘添加一个右键弹出菜单
			PopupMenu popMenu = new PopupMenu();

			MenuItem itemOpen = new MenuItem("OPEN");
			itemOpen.addActionListener(e -> frame.setVisible(true));

			MenuItem itemHide = new MenuItem("HIDE");
			itemHide.addActionListener(e -> frame.setVisible(false));

			MenuItem itemStats = new MenuItem("Stats");
			itemStats.addActionListener(e -> new Setting().setVisible(true));

			MenuItem itemBook = new MenuItem("Book");
			itemBook.addActionListener(e -> new AccounterGui(user));

			MenuItem itemExit = new MenuItem("EXIT");
			itemExit.addActionListener(e -> System.exit(0));

			popMenu.add(itemOpen);
			popMenu.add(itemHide);
			popMenu.add(itemStats);
			popMenu.add(itemBook);
			popMenu.add(itemExit);

			// 设置托盘图标
			ImageIcon icon = new ImageIcon("girl1.png");
			Image image = icon.getImage().getScaledInstance(icon.getIconWidth(), icon.getIconHeight(),
					Image.SCALE_DEFAULT);

			TrayIcon trayIcon = new TrayIcon(image, "不破爱花", popMenu);
			trayIcon.setImageAutoSize(true); // 自适应尺寸，这个属性至关重要

			try {
				tray.add(trayIcon);
			} catch (AWTException e1) {
				e1.printStackTrace();
			}
		}
	}

}
