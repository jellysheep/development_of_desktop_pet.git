package User;

public class User {
	private String userName;
	private String password;
	public String zu;
	
	public User(String userName, String password, String zu) {
		super();
		this.userName = userName;
		this.password = password;
		this.zu = zu;
	}
	public User(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}
	public User(String userName) {
		this.userName = userName;
	}
	public User() {
		// TODO Auto-generated constructor stub
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getZu() {
		return zu;
	}
	public void setZu(String zu) {
		this.zu = zu;
	}
	@Override
	public String toString() {
		return "User [userName=" + userName + ", password=" + password + ", zu=" + zu + "]";
	}

}