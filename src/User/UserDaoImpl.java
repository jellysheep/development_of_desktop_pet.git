package User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class UserDaoImpl {

	private List<User> userList;
	
	public UserDaoImpl(){
		init();
	}
	
//	public UserDaoImpl(Map<String, User> userMap){
//		if (userMap==null || userMap.isEmpty()){
//			init();
//		}
//		this.userMap = userMap;
//			

	public static List<User> findAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<User> list = new ArrayList<>();
		try {
			connection = DBManager.getConnection();
			String sql = "SELECT * FROM users";
			statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				User user = new User(resultSet.getString("name"), resultSet.getString("pwd"));
				list.add(user);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.closeAll(connection, statement, resultSet);
		}
		
		return list;
	}
	public void init(){
		userList=findAll();
//		userMap = new HashMap<String, User>();
//		userMap.put("1", new User("1","1"));
	}
	
//	public Map<String, User> getUserMap() {
//		return userMap;
//	}
//
//	public void setUserMap(Map<String, User> userMap) {
//		this.userMap = userMap;
//	}

	public User isLogin(String userName, String password) {	
		userList=findAll();
		User user1=new User(userName, password);
		Iterator it = userList.iterator();
		while(it.hasNext()){
			User user=(User) it.next();
		   // System.out.print(user.getUserName());
		//   System.out.println(user.getPassword());
		if (userName.equals(user.getUserName())&&password.equals(user.getPassword())){
				return user1;
			}
		}
		
		
		return null;
	}
	public static User findByName(String name) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DBManager.getConnection();
			String sql = "SELECT * FROM users WHERE name = ?;";
			statement = connection.prepareStatement(sql);
			statement.setString(1, name);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				return new User(
						resultSet.getString("name"), resultSet.getString("pwd"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.closeAll(connection, statement, resultSet);
		}
		return null;
	}
	public boolean insert(User user) {//注册
		Connection connection = null;
		PreparedStatement statement = null;
		if(UserDaoImpl.findByName(user.getUserName())!=null) {
			return false;
		}
		try {
			connection = DBManager.getConnection();
			String sql = "INSERT INTO users(name, pwd,zu) VALUES ( ?, ?,?);";
			statement = connection.prepareStatement(sql);
			statement.setString(1, user.getUserName());
			statement.setString(2, user.getPassword());
			statement.setString(3, user.getZu());
			int count = statement.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.closeAll(connection, statement);
		}
		return false;
	}
}

