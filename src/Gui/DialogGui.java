package Gui;

import pets.TestBody;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import Reminder.Talk;
import User.User;

public class DialogGui {
	public static JFrame frame;
	private static JLabel jLabel;

	public DialogGui() {
		Talk txt = new Talk();

		frame = new JFrame();// 框架
		frame.setAlwaysOnTop(true);

		ImageIcon img = new ImageIcon("对话框3.png");// 这是背景图片
		JLabel imgLabel = new JLabel(img);// 将背景图放在标签里。

		String txt1 = txt.getWord();
		JLabel jLabel2 = new JLabel(txt1);
		JLabel jLabel1 = new JLabel(" ");
		jLabel2.setFont(new Font("宋体", Font.BOLD | Font.ITALIC, 25));
		jLabel2.setBounds(420, 70, 300, 30);
		// jLabel2.setForeground(Color.red);

		frame.getLayeredPane().add(imgLabel, new Integer(Integer.MIN_VALUE));
		imgLabel.setBounds(0, 0, img.getIconWidth(), img.getIconHeight());
		Container cp = frame.getContentPane();
		cp.setLayout(new BorderLayout());

		cp.add(jLabel2);
		cp.add(jLabel1);

		((JPanel) cp).setOpaque(false);

		frame.setUndecorated(true); // 取消窗口标题栏
		frame.setBackground(new Color(0, 0, 0, 0));// 背景frame
		frame.setVisible(true);
		frame.setSize(800, 500);

	}

	public void setV() {
		frame.setVisible(false);// 隐藏掉登陆界面
		
	}

}
