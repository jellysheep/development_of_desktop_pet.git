package Gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Account.Accounter;
import Account.AccounterDao;
import User.User;

public class AccounterGui extends javax.swing.JFrame {
	
	public AccounterGui() {
		User user=new User();
		initInterGui(user);
	}
    //  登录界面GUI
	//  测试测试测试
	//  测试测试测试
	public AccounterGui(User user) {
		initInterGui(user);
		
	}
	public void initInterGui(User user) {
		
		JFrame inter = new JFrame("记   账");
         //inter.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		inter.setLayout(null); // 清除布局函数
		//inter.setResizable(false); // 设置窗体大小不可变
		inter.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		//inter.setBounds((int)screenSize.height, (int)screenSize.width,300,500);
		
		inter.setLayout(new BorderLayout()); // 新建BorderLayout布局
		//inter.setLocation((int)screenSize.height-400, (int)screenSize.width-400);

		// 背景
		JPanel panel0 = new JPanel(new FlowLayout());
		ImageIcon icon = new ImageIcon("image/logo.png");
		JLabel label = new JLabel(icon);
		panel0.add(label);
		inter.add(panel0, BorderLayout.NORTH);

		// 账号密码框
		JPanel panel1 = new JPanel(new GridLayout(2, 1, 0, 20));
		panel1.setBorder(new EmptyBorder(10, 10, 10, 60));

		JLabel jl1 = new JLabel("收 入"); // 创建账号标签
		jl1.setFont(new Font("微软雅黑", Font.BOLD, 16)); // 设置字体、样式、大小
		jl1.setHorizontalAlignment(JTextField.CENTER); // 设置水平居中
		JTextField jt1 = new JTextField(10); // 创建文本框
		jt1.setFont(new Font("微软雅黑", Font.PLAIN, 16)); // 设置字体、样式、大小

		JLabel jl2 = new JLabel("支 出"); // 创建密码标签
		jl2.setFont(new Font("微软雅黑", Font.BOLD, 16)); // 设置字体、样式、大小
		jl2.setHorizontalAlignment(JTextField.CENTER); // 设置居中
		JPasswordField jt2 = new JPasswordField(10); // 创建密码文本框
		//jt2.setEchoChar('*'); // 设置回显字符为 *
		jt2.setFont(new Font("微软雅黑", Font.PLAIN, 16)); // 设置字体、样式、大小

		panel1.add(jl1);
		panel1.add(jt1);
		panel1.add(jl2);
		panel1.add(jt2);

		inter.add(panel1, BorderLayout.CENTER);

		// 登录按钮
		JPanel panel2 = new JPanel(new FlowLayout());

		JButton jb1 = new JButton("记入");
		// 登录按钮触发事件
		jb1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				jb1.setText("正在记入......");
				String s1= jt1.getText();
				Double in = Double.parseDouble(s1);
				s1 = new String(jt2.getPassword());		
				Double out =Double.parseDouble(s1);
				AccounterDao acd=new AccounterDao();
				Accounter ac=new Accounter(user);
	        	//User user=new User("1","1");
	        	 ac=acd.findByName(user);
	        	 if(AccounterDao.update(ac, in, out)) {
						JOptionPane.showMessageDialog(null, "记入成功！");
	        	 }
	        	 jb1.setText("记入");
			}
		});	
		JButton jb2 = new JButton("查看记录");
		// 登录按钮触发事件
		jb2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				jb2.setText("正在查看......");
				AccounterDao acd=new AccounterDao();
				Accounter ac=new Accounter();
	        	//User user=new User("1","1");
	        	 ac=acd.findByName(user);
//				String s1= jt1.getText();
//				Double in = Double.parseDouble(s1);
//				s1 = new String(jt2.getPassword());		
//				Double out =Double.parseDouble(s1);
	        	 //System.out.println(ac.getIncome());
				JOptionPane.showMessageDialog(null, "支出："+ac.getOut()+" 收入："+ac.getIncome());
				jb2.setText("查看记录");
			}
			
		});	
        
		panel2.add(jb1);
		panel2.add(jb2);

		inter.add(panel2, BorderLayout.SOUTH);

		// Display the window.
		inter.pack();
		inter.setVisible(true);
		// 设置窗口居中显示
		//SettingWindow.setFrameCenter(inter);

	}

}
