package Pet;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import User.DBManager;
import User.User;
import User.UserDaoImpl;

public class PetDao implements PetBaseDao  {
	String name;
	int Liking;
	int age;
	String PetName;

	String sex;
	String zu;

	public PetDao(String name, int age, String petName, String sex, String zu) {
		super();
		this.Liking=10;
		this.name = name;
		this.age = age;
		PetName = petName;
		this.sex = sex;
		this.zu = zu;
	}

	public PetDao(String name, int Liking, int age, String PetName, String sex) {
		super();
		this.name = name;
		this.Liking = Liking;
		this.age = age;
		this.PetName = PetName;
		this.sex = sex;
	}

	public PetDao(String name, int age, String petName, String sex) {
		super();
		this.name = name;
		this.age = age;
		PetName = petName;
		this.sex = sex;
	}

	public PetDao() {
		
	}

	public PetDao(User user) {
		this.name = user.getUserName();// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLiking() {
		return Liking;
	}

	public void setLiking(int liking) {
		Liking = liking;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPetName() {
		return PetName;
	}

	public void setPetName(String petName) {
		PetName = petName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getZu() {
		return zu;
	}

	public void setZu(String zu) {
		this.zu = zu;
	}

	public boolean delete(PetDao user) {
		Connection connection = null;

		PreparedStatement statement = null;
		try {
			connection = DBManager.getConnection();
			String sql = "DELETE FROM pet WHERE name = ?;";
			statement = connection.prepareStatement(sql);
			statement.setString(1, user.getName());
			int count = statement.executeUpdate();

			return count > 0;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.closeAll(connection, statement);
		}

		return false;
	}

	public boolean insert(PetDao pet) {// 注册
		Connection connection = null;
		PreparedStatement statement1 = null;
		
		try {
			connection = DBManager.getConnection();

			String sql = "INSERT into pet VALUES(?,?,?,?,?,?);";

			statement1 = connection.prepareStatement(sql);

			statement1.setString(1, pet.getName());
			statement1.setInt(2, pet.getLiking());
			statement1.setInt(3, pet.getAge());
			statement1.setString(4, pet.getPetName());
			statement1.setString(5, pet.getSex());
			statement1.setString(6, pet.getZu());
                
			statement1.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.closeAll(connection, statement1);
		}
		return false;
	}

	/*
	 * public static boolean update(Accounter ac, Double in, Double out) {
	 * //System.out.println(ac.income); ac.income=in+ac.getIncome();
	 * ac.out=out+ac.getOut(); //System.out.print(ac.income); AccounterDao acc=new
	 * AccounterDao(); acc.delete(ac); acc.insert(ac); return true;
	 * 
	 * }
	 */
	public static int addLiking(PetDao pet, int num) {
		PetDao pt = null;
		pet = pet.findByName(pet);
		
		pet.delete(pet);
	
		pet.Liking += num;
		
		pet.insert(pet);
System.out.println("4");
		return pet.Liking;
	}

	public PetDao findByName(PetDao pet1) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		List<PetDao> list = new ArrayList<>();
		try {
			connection = DBManager.getConnection();
			String sql = "SELECT * FROM pet";
			statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				PetDao pet = new PetDao(resultSet.getString("name"), resultSet.getInt("liking"),
						resultSet.getInt("age"), resultSet.getString("petname"), resultSet.getString("sex"));
//				pet.setLiking(resultSet.getInt("linking"));
//				pet.setName(resultSet.getString("petname"));
				list.add(pet);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.closeAll(connection, statement, resultSet);
		}

		Iterator<PetDao> it = list.iterator();
		while (it.hasNext()) {
			PetDao acc = (PetDao) it.next();

			if (acc.getName().equals(pet1.getName())) {
				// System.out.print(acc.getOut());
				// acc.toString();

				return acc;
			}
		}
		return null;
	}

}
