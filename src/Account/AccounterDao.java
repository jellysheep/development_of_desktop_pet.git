package Account;

import java.io.ObjectInputStream.GetField;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import User.DBManager;
import User.User;

public class AccounterDao {
	private List<Accounter> accountList;

	public AccounterDao() {
		;
	}
	@SuppressWarnings("unlikely-arg-type")
	public static Accounter findByName(User user) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		//Accounter accounter= new Accounter()
		List<Accounter> list = new ArrayList<>();
		try {
			connection = DBManager.getConnection();
			String sql = "SELECT * FROM account";
			statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				Accounter accounter = new Accounter(resultSet.getString("name"),resultSet.getDouble("out"),resultSet.getDouble("in"));
				list.add(accounter);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.closeAll(connection, statement, resultSet);
		}

		Iterator<Accounter> it = list.iterator();
		while(it.hasNext()){
			Accounter acc=(Accounter) it.next();

		if (acc.getUserName().equals(user.getUserName())){
			System.out.print(acc.getOut());
			acc.toString();
				return acc;
			}
		}		return null;	
	}
	
	public static boolean delete(Accounter ac) {
		Connection connection = null;
//		ac.setIncome(ac.getIncome()+in);
//		ac.setOut(ac.getOut()+out);
		PreparedStatement statement = null;
		try {
			connection = DBManager.getConnection();
//			
//			
//			int count = statement.executeUpdate();
//			return count > 0;
		
					String sql = "DELETE FROM account WHERE name = ?;";
					statement = connection.prepareStatement(sql);
					statement.setString(1,ac.getUserName() );
					int count = statement.executeUpdate();
//					 sql = "INSERT INTO account(name, out,in) VALUES ( ?, ?,?);";
//					PreparedStatement statement1 = connection.prepareStatement(sql);
//					statement.setString(1, user.getUserName());
//					statement.setString(2, user.getPassword());
//					statement1.setString(1, ac.getUserName());
//					statement1.setDouble(2, ac.getOut());
//					statement1.setDouble(3, ac.getIncome());
//					count = statement.executeUpdate();
					return count > 0;
				} catch (Exception e) {
					e.printStackTrace();
				}finally {
					DBManager.closeAll(connection, statement);
				}
	
		return false;
	}


	public static boolean insert(Accounter ac) {//注册
		Connection connection = null;
		PreparedStatement statement1 = null;
		try {
			connection = DBManager.getConnection();

			String sql = "INSERT into account VALUES(?,?,?);";
			//String sql = "INSERT INTO account(out, in,name) VALUES (?, ?, ?);";
			 statement1 = connection.prepareStatement(sql);

				statement1.setString(3, ac.getUserName());
				statement1.setDouble(1, ac.getOut());
				statement1.setDouble(2, ac.getIncome());
				
			statement1.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.closeAll(connection, statement1);		}
		return false;
	}
	public static boolean update(Accounter ac, Double in, Double out) {
		System.out.println(ac.income);
		ac.income=in+ac.getIncome();
		ac.out=out+ac.getOut();
		System.out.print(ac.income);
		AccounterDao acc=new AccounterDao();
		acc.delete(ac);
		acc.insert(ac);
		return true;
		
	}
}
